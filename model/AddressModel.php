<?php

/**
 * FileName : AddressModel.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

/**
 * AddressModel class to perform operation on address table
 * 
 */
class AddressModel extends Database
{
    /**
     * Constructor function to initialize necessary details passed
     * 
     * @param mixed $details contains details
     */
    public function __construct($details)
    {
        $this->details = $details;
        parent::__construct();
    }
    
    /**
     * To insert address details into database
     * 
     * @return mixed
     */
    public function insertAddress()
    {
        $sessionData['user_id'] = $_SESSION['user_id'];
        $addressDetails = array_merge($this->details, $sessionData);
        $result = $this->insert(ADDRESS_TABLE, $addressDetails);
        return $result;
    }
    
    /**
     * To retrieve the address book details of user
     * 
     * @return array $record
     */
    public function getList()
    {
        $databaseObject = new Database();
        $fields = [array('address_id','name', 'address', 'city', 'mobile', 'user_id'),
                    array('country_name'),array('state_name')];
        $tables = [ADDRESS_TABLE,COUNTRY_TABLE,STATE_TABLE];
        $join = ['type'=>"INNER JOIN"];
        $where = ['user_id'=> $this->details];
        $orderBy = ['name'];
        $record = $databaseObject->join($fields, $tables, $join, $where, $orderBy);
        for ($iteration = 0; $iteration<sizeof($record); $iteration++) {
            $record[$iteration]['address'] .= ','.$record[$iteration]['city']
                    .','.$record[$iteration]['state_name']
                    .','.$record[$iteration]['country_name'];
            unset($record[$iteration]['city'], $record[$iteration]['state_name'],
                    $record[$iteration]['country_name'],
                    $record[$iteration]['user_id']);
        }
        return $record;
    }
    
    /**
     * To update address details in database
     * 
     * @param string $address_id contains the address_id to be updated
     * 
     * @return boolean $result
     */
    public function updateAddress($address_id)
    {
        $key = array("address_id"=>$address_id);
        $result = $this->update(ADDRESS_TABLE, $this->details, $key);
        return $result;
    }
    
    /**
     * To validate the address details
     * 
     * @return mixed
     */
    public function validate()
    {
        array_pop($this->details);
        foreach ($this->details as $key => $value) {
            if (empty($value)) {
                return $key." is empty";
            } 
        }
        return true;
    }
    
    /**
     * To delete the address details from database
     * 
     * @return boolean $result
     */
    public function deleteDetails()
    {
        $key = array("address_id"=> $this->details);
        $result = $this->delete(ADDRESS_TABLE, $key);
        return $result;
    }
}