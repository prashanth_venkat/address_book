<?php

/**
 * FileName : AuthModel.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

/**
 * AuthModel class to perform authentication and login 
 * 
 * extends Database class
 */
class AuthModel extends Database
{
    /**
     * Constructor function to initialize necessary details passed
     * 
     * @param mixed $details contains the necessary details
     */
    public function __construct($details)
    {
        $this->details = $details;
        parent::__construct();
    }
    
    /**
     * Function to validate registration details
     * 
     * @return mixed
     */
    public function registerValidate()
    {
        if (empty($this->details['user_name'])) {
            $registerError = array("isValid" => 0, "message" => "Name Required");
            return $registerError;
        } elseif (!preg_match('/^[a-z A-Z ]*$/', $this->details["user_name"])) {
            $registerError = array("isValid" => 0, "message" => "Enter only characters for Name");
            return $registerError;
        }
        $registerError = $this->validate();
        if ($registerError['isValid']) {
            if (empty($this->details['confirmPassword'])) {
                $registerError = array("isValid" => 0, "message" => "Confirm Password Required");
            } elseif ($this->details['confirmPassword'] != $this->details['password']) {
                $registerError = array("isValid" => 0, "message" => "Password Mismatch");                
            } 
        }
        return $registerError;
    }
    
    /**
     * Function to validate details
     * 
     * @return array $validationError
     */
    private function validate()
    {
        $validationError = array("isValid" => 1, "message" => "null");
        if (empty($this->details['email'])) {
            $validationError = array("isValid" => 0, "message" => "Email Required");
        } elseif (!filter_var($this->details['email'], FILTER_VALIDATE_EMAIL)) {
            $validationError = array("isValid" => 0, "message" => "Invalid Email format");
        } elseif (empty($this->details['password'])) {
            $validationError = array("isValid" => 0, "message" => "Password Required");
        } elseif (!preg_match ('/^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/', $this->details['password'])) {
            $validationError = array("isValid" => 0, "message" => "Invalid format for Password");
        }
        return $validationError;
    }
    
    /**
     * Function to validate Login details
     * 
     * @return mixed
     */
    public function loginValidate()
    {
        $loginError = $this->validate();
        if ($loginError['isValid']) {
            $validUser = $this->getValidUserDetails();
            if ($validUser['user_id']) {
                $validUser['isValid'] = 1;
                return $validUser;
            } else {
                $loginError = array(
                    'isValid' => 0,
                    'message' => "Email and Password didn't match"
                );
                return $loginError;
            }
        }
        return $loginError;
    }
    
    /**
     * To store the user details
     * 
     * @return boolean
     */
    public function store()
    {
        $details = $this->details;
        $md5password = array("password" => md5($details["password"]));
        $details = array_replace($details, $md5password);
        array_pop($details);
        array_pop($details);
        $result = $this->insert(USERS_TABLE, $details);
        return $result;
    }
    
    /**
     * Function to get valid user details while logging in
     * 
     * @return array $userDetails 
     */
    public function getValidUserDetails()
    {   
        $userDetails[] = array();
        $md5password = array("password" => md5($this->details["password"]));
        $this->details = array_replace($this->details, $md5password);
        array_pop($this->details);
        $columns = array('user_name', 'user_id','email');
        $condition = array(
            'email' => $this->details['email'],
            'password' => $this->details['password']
            );
        $result = $this->get($columns, USERS_TABLE, $condition);
        if (isset($result[0])) {
            $userDetails = $result[0];
            return $userDetails;
        }
    }
}