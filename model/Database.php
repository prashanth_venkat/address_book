<?php

/**
 * FileName : Database.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */


/**
 * Database class to connect to database and perform operations on db
 * 
 */
class Database
{
    private $conn;
    private $logger;
    
    /**
     * Constructor function to establish database connection
     * 
     *  @return void 
     */
    public function __construct() 
    {
        $this->conn = new mysqli(DBHOST, DBUSER, DBPASSWORD, DBNAME);
        $this->logger = new Logger();
    }

    /**
     * To retrieve details from the database
     * 
     * @param array  $columns   columns of the table
     * @param string $table     table name
     * @param array  $condition condition to check
     * 
     * @return array $records
     */
    protected function get($columns,$table,$condition)
    {
        $fields = implode(',', $columns);
        $query = "SELECT $fields FROM $table";
        if (!empty($condition)) {
            $query .= " WHERE ";
            foreach ($condition as $key => $value) {
                $query .= $key ." = '". $value ."' AND ";
            }
            $query = substr($query, 0, -5);
        }
        $this->logger->logInfo($query);
        $result = $this->conn->query($query);
        $records = array();
        while ($row = $result->fetch_assoc()) {
            $records[] = $row;
        }
        return $records;
    }
    
    /**
     * Function to insert details in database
     * 
     * @param string $table   contains the table name to insert
     * @param array  $details contains key value pair details
     * 
     * @return boolean $result
     */
    protected function insert($table, $details)
    {
        $query = sprintf(
            'INSERT INTO '.$table.' (%s) VALUES ("%s")',
            implode(',', array_keys($details)),
            implode('","', array_values($details))
        );
        $this->logger->logInfo($query);
        $result = $this->conn->query($query);
        return $result;
    }
    
    
    /**
     * Function to update details
     * 
     * @param string $table      contains the table name
     * @param array  $details    contains key value pair details
     * @param array  $primarykey contains the primary key of the table
     * 
     * @return boolean $result
     */
    protected function update($table, $details, $primarykey)
    {
        foreach ($details as $field => $val) {
            $columns[] = "$field = '$val'";
        }
        $query = "UPDATE $table SET " .implode(', ', $columns)." WHERE ";
        foreach ($primarykey as $key => $value) {
            $query .= $key." = ".$value;
        }
        $this->logger->logInfo($query);
        $result = $this->conn->query($query);
        return $result;
    }
    
    /**
     * Function to delete details from table
     * 
     * @param string $table      contains table name
     * @param array  $primarykey contains the primary key of the table
     * 
     * @return boolean $result
     */
    protected function delete($table,$primarykey)
    {
        $query = "DELETE FROM $table WHERE ";
        $query .= key($primarykey). " IN ("; 
        foreach ($primarykey as $key => $value) {
            $query .= $value.", ";
        }
        $query = substr($query, 0, -2);
        $query .= ")";
        $this->logger->logInfo($query);
        $result = $this->conn->query($query);
        return $result;
    }
    
    
    /**
     * To retrieve the address details from the database
     * 
     * @param array $fields  contains columns of tables to be retrieved
     * @param array $tables  contains the table names
     * @param array $join    contains join type
     * @param array $where   contains condition to check 
     * @param array $orderBy contains order by column 
     *
     * @return array $addressList
     */
    protected function join($fields,$tables,$join,$where,$orderBy=[])
    {
        $col1 = "a.".implode(", "."a.", $fields[0]);
        $col2 = "c.".implode(", "."c.", $fields[1]);
        $col3 = "s.".implode(", "."s.", $fields[2]);
        $on = ['country_id','state_id',];
        $joincond1 = "a.".$on[0]." = c.".$on[0];
        $joincond2 = "a.".$on[1]." = s.".$on[1];
        $wherecond = key($where)." = ".array_shift($where);
        $query = "SELECT ".$col1.", ".$col2.", ".$col3." FROM ".$tables[0].""
                . " AS a $join[type] ".$tables[1]." AS c "." ON ".$joincond1.""
                . " $join[type] ".$tables[2]." AS s ON ".$joincond2." WHERE ".$wherecond;
        if (!empty($orderBy)) {
            $orderby = "a.".$orderBy[0];
            $query .= " ORDER BY ".$orderby;
        }
        $this->logger->logInfo($query);
        $records = $this->conn->query($query);
        while ($result = $records->fetch_assoc()) {
            $addressList[] = $result;
        }
        if (!empty($addressList)) {
            return $addressList;
        }
    }
    
    /**
    * Desctructor function to destroy database connection
    *
    * @return void
    */
    public function __destruct()
    {
        mysqli_close($this->conn);
    }
}
