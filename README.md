# README #

### Description ###
The application is an address book, in which users can store address details of their friends and relatives.
The application is done in MVC design pattern in php.

### Pre requisites ###
Requirements:
LAMP server:
PHP version 5.5
Apache2 web server

Database:
Consists of 4 tables
users, address_list, country and state table.



### Contact ###

owner : <prashanth.venkat@aspiresys.com>
ref : <shatheesh.jayaram@aspiresys.com>
