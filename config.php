<?php

define("ROOT_DIRECTORY", $_SERVER['DOCUMENT_ROOT']."/address_book");

define("INDEX", "http://localhost/address_book");

define("LOG_FILE", "/var/www/html/address_book/logger/errors.log");
define("QUERY_LOG_FILE","/var/www/html/address_book/logger/queries.log");
define("DIR","http://localhost/address_book");

/**
 * For Fatal errors   give 1
 * For Warning errors give 2
 * For Notice errors  give 8
 */
define("ERROR_LEVEL", 1);

define("DBHOST", "localhost");
define("DBNAME", "address_book");
define("DBUSER", "prashanth");
define("DBPASSWORD", "aspire@123");

define("ADDRESS_TABLE", "address_list");
define("COUNTRY_TABLE", "country");
define("STATE_TABLE", "state");
define("USERS_TABLE", "users");