/**
 * To validate login details
 * @type type
 */
$(document).ready(function() {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        return regexpr.test(value);
        }, "Provide valid details");
    $('#login').validate({
       rules: {
           email: {
                required : true,
                email    : true,
                maxlength: 64,
                regex    : /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/
            },
            password: {
                required : true,
                minlength: 8
            }
       } 
    });
});