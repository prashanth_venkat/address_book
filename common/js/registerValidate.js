/**
 * To validate registration details
 */
$(document).ready(function() {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        return regexpr.test(value);
        }, "Provide valid details");
    $('#register').validate({
        rules: {
            user_name: {
                required : true,
                minlength: 3,
                maxlength: 20,
                regex    : /^[a-zA-Z ]*$/
            },
            email: {
                required : true,
                email    : true,
                maxlength: 64
            },
            password: {
                required : true,
                minlength: 8,
                maxlength: 20,
                regex    : /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/
            },
            confirmPassword: {
                required : true,
                equalTo  : "#password"
            }
        },
        messages: {
            confirmPassword : "Mismatch"
        }
    });

    $("#email").blur(function() {
        var emailData = $(this).val();
        if (emailData.match(/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/)) {
            $.ajax({
                type    : "POST",
                url     : "./availability",
                data    : "email="+emailData,
                datatype: "text",
                success : function(text) {
                    document.getElementById("emailError").innerHTML = text;
                    if(text === "Already Exists"){
                        document.getElementById("signUp").disabled = true;
                    } else{
                        document.getElementById("signUp").disabled = false;   
                    }
                }
            });
        } else {
            document.getElementById("signUp").disabled = true;
        }
    });  
});
