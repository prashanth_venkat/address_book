/**
 * Ajax request to display the states of corresponding country selected
 * 
 * @param {string} state_id
 *
 * @returns {response}
 */
function dropdownRequest(state_id)
{
    var country = $(country_id).val();
    $.ajax({
        type     : "POST",
        url      : "/address_book/address/getState",
        data     : "country=" + country,
        datatype : "json",
        success : function(text) {
            var jsonData = JSON.parse(text);
            for (var data = 0; data<jsonData.length-1; data++) {
                if (state_id == jsonData[data].state_id) {
                    document.getElementById("state_id").appendChild(new Option(jsonData[data].state_name, jsonData[data].state_id,true,true)).value=jsonData[data].state_id;
                } else {
                    document.getElementById("state_id").appendChild(new Option(jsonData[data].state_name, jsonData[data].state_id)).value=jsonData[data].state_id;
                }
            }
        }
    });
    var child = document.getElementById("state_id");
    child.innerHTML = "";
}

/**
 * To delete the selected records
 * 
 * @returns {response} and redirect
 */
function deleteRecords()
{
    var result = confirm("Are you sure to delete the selected records?");
    if (result) {
        var address_id = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();

        $.ajax({
            type    : "POST",
            url     : "/address_book/address/delete",
            data    : "address_id=" + address_id,
            success : function(text) {
                if (text) {
                    location.href = "/address_book/address/list?message=Succesfully deleted.";
                } else {
                    location.href = "/address_book/address/list?message=There was some problem while deleting";
                } 
            }
        
        });
    }
}

/**
 * To validate address details
 */
$(document).ready(function() {
    $.validator.addMethod("regex", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Provide valid details");
    $('#address').validate({
        errorElement: "div",
        errorPlacement: function(error, element) {
            element.after(error);
        },
       rules: {
            name: {
                required : true,
                minlength: 3,
                maxlength: 20,
                regex    : /^[a-z A-Z ]*$/
            },
            age: {
                required: true,
            },
            address: {
                required : true,
                minlength: 8,
                maxlength: 120,
                regex    : /^[a-z A-Z 0-9 +@,#\n ]*$/
            },
            mobile: {
                required : true,
                minlength: 10,
                maxlength: 13,
                regex    : /^[a-z A-Z 0-9 +]*$/
            },
            country_id: {
                required: true,  
            },
            state_id: {
                required: true,  
            },
            city: {
                required : true,
                maxlength: 20,
                regex    : /^[a-z A-Z ]*$/
            }
        }
    });
});