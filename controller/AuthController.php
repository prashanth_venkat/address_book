<?php

/**
 * FileName : AuthController.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

require_once "config.php";
session_start();
require 'BaseController.php';


/**
 * AuthController class to authenticate while logging in and registering
 * 
 * extends BaseController
 */
Class AuthController extends BaseController
{
    private $logger;
    
    /**
     * Constructor function to intialize necessary details
     */
    public function __construct()
    {
        parent::__construct();
        $this->logger = new Logger();

    }

    /**
     * To login to the application
     * 
     * render/redirect
     */
    public function loginAction()
    {         
        if (isset($_POST["login"])) {
            $auth = new AuthModel($_POST);
            $validateResult = $auth->loginValidate();
            if ($validateResult['isValid']) {
                $_SESSION["user_name"] = $validateResult["user_name"];
                $_SESSION["user_id"] = $validateResult["user_id"];
                $_SESSION["email"] = $validateResult["email"];
                $_SESSION["loggedIn"] = true;
                $this->redirect("/address/list");
            } else {
                $this->logger->logError($validateResult['message']);
                $this->redirect("/auth/login?message=", $validateResult['message']);
            }
        } elseif (isset($_SESSION["loggedIn"])) {
            $this->logger = new Logger("User ".$_SESSION["user_id"]." is already logged In");
            $this->redirect("/address/list");
        } else {
            $this->render("/view/login");
        }
    }
    
    /**
     * To register the user 
     * 
     * render/redirect
     */
    public function registerAction()
    {   
        if (isset($_POST["signUp"])) {
            $auth = new AuthModel($_POST);
            $validateResult = $auth->registerValidate();
            if ($validateResult['isValid']) {
                if ($auth->store()) {
                    $this->logger->logInfo("A new user registered successfully.");
                    $this->redirect("/auth/login?message=", "Log in to continue");
                }
            } else {
                $this->logger->logError($validateResult['message']);
                $this->redirect("/auth/register?message=", $validateResult['message']);
            }
        } else {
            $this->render("/view/register");
        }
    }
    
    /**
     * Function to get the availability while registering
     * 
     * echoes the ajax response
     */
    public function availabilityAction()
    {
        $helperObject = new Helper();
        $availability = $helperObject->getAvailabilityStatus($_POST['email']);
        echo $availability;
    }
    
    /**
     * To logout of the application
     * 
     * render
     */
    public function logoutAction()
    {
        session_destroy();
        $this->loginAction();
    }
    
    /**
     * To log the errors in log file
     */
    public function endScript()
    {
        if (error_get_last()) {
            $this->logger->logError(error_get_last());
        }
    }
}

register_shutdown_function(array(new AuthController(),"endScript"));
//include "b.php";