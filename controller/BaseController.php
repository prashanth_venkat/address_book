<?php

/**
 * FileName : BaseController.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

require_once "config.php";

/**
 * BaseController class to redirect and render pages
 * 
 * render/redirect
 */
class BaseController
{
    /**
     * Constructor to include necessary documents
     * 
     */
    public function __construct() {
        spl_autoload_register(function ($class_name) {
            if (is_file(ROOT_DIRECTORY.'/model/'.$class_name.'.php')) {
                require_once ROOT_DIRECTORY.'/model/'.$class_name.'.php';
            } elseif (is_file(ROOT_DIRECTORY.'/helper/'.$class_name.'.php')) {
                require_once ROOT_DIRECTORY.'/helper/'.$class_name.'.php';
            } elseif (is_file(ROOT_DIRECTORY.'/logger/'.$class_name.'.php')) {
                require_once ROOT_DIRECTORY.'/logger/'.$class_name.'.php';
            } else {
                require_once $class_name . '.php';
            }
        });
    }
    
    /**
     * To redirect to the page
     * 
     * @param string $page    contains controller and action
     * @param string $message message to display if any
     * 
     * redirects
     */
    protected function redirect($page, $message = "")
    {
            header("Location:".INDEX.$page.$message);
    }
    
    /**
     * To redirect to the page
     * 
     * @param string $page    contains controller and action
     * @param array  $details contains address details
     * 
     * render
     */
    protected function render($page, $details="")
    {
        include ROOT_DIRECTORY.$page.".php";
    }
}