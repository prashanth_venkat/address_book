<?php

/**
 * FileName : AddressController.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

require_once "config.php";
session_start();
require 'BaseController.php';

/**
 * AddressController class to add,list,update,delete address details
 * 
 * extends BaseController class
 * 
 */
class AddressController extends BaseController
{
    private $logger;
    /**
     * To initialize and include necessary documents
     */
    public function __construct()
    {
        parent::__construct();
        $this->logger = new Logger();
    }

    /**
     * To list all address details in address book of the user
     * 
     * renders address list
     */
    public function listAction()
    {
        if (!isset($_SESSION["loggedIn"])) {
            $this->redirect("/auth/login");
        } else {
            $address = new AddressModel($_SESSION['user_id']);
            $addressDetails = $address->getList();
            $this->render("/view/address_list", $addressDetails);
        }
    }
    
    /**
     * To add address details
     * 
     * renders/redirects
     */
    public function addAction()
    {
        if (!isset($_SESSION["loggedIn"])) {
            $this->redirect("/auth/login");
        } elseif (isset($_POST['addAddress'])) {
            $address = new AddressModel($_POST);
            $validateResult = $address->validate();
            if (is_bool($validateResult)) {
                $success = $address->insertAddress();
                if ($success) {
                    $this->redirect("/address/list");
                } else {
                    $this->logger->logError("Problem while Adding Address details.");
                    $this->redirect("/address/add?message=Sorry!Please try again.");
                }
            } else {
                $this->logger->logError($validateResult." while adding");
                $this->redirect("/address/add?error=", $validateResult);
            }
        } else {
            $this->render("/view/address");
        }
    }
    
    /**
     * To update the address details in database
     * 
     * renders/redirects
     */
    public function updateAction()
    {
        $uri = $_SERVER["REQUEST_URI"];
        $index = strrpos($uri, "/");
        $index ++;
        $id = substr($uri, $index);
        if (!isset($_SESSION["loggedIn"])) {
            $this->redirect("/auth/login");
        } elseif (isset ($_POST['updateAddress'])) {
            $address = new AddressModel($_POST);
            $validateResult = $address->validate();
            if (is_bool($validateResult)) {
                $success = $address->updateAddress($id);
                if ($success) {
                    $this->redirect("/address/list");
                } else {
                    $this->logger->logError("Problem while Updating");
                    $this->redirect("/address/list?message=Sorry!Please try again.");
                }
            } else {
                $this->loggererror($validateResult." while updating");
                $this->redirect("/address/add?error=", $validateResult);
            }
        }
    }
    
    /**
     * To retrieve state list for the particular country
     * 
     * echoes country list
     */
    public function getStateAction()
    {
        $helperObject = new Helper();
        $result = $helperObject->getState($_POST['country']);
        echo json_encode($result);
    }
    
    /**
     * To edit the address details
     * 
     * renders/redirects
     */
    public function editAction()
    {
        if (isset($_SESSION["loggedIn"])) {
            $helper = new Helper();
            $uri = $_SERVER["REQUEST_URI"];
            $index = strrpos($uri, "/");
            $index ++;
            $id = substr($uri, $index);
            $address = $helper->getAddress($id, $_SESSION['user_id']);
            if (!empty($address)) {
                $this->render("/view/edit", $address);
            } else {
                $this->logger->logError("Access denied for user".$_SESSION["user_id"]);
                $this->render("/view/denied");
            }
        } else {
            $this->redirect("/auth/login");
        }
    }
    
    
    /**
     *To delete the address details 
     * 
     * echoes boolean 
     */
    public function deleteAction()
    {
        if (isset($_POST["address_id"])) {
            $address = new AddressModel($_POST["address_id"]);
            $obj  = $address->deleteDetails();
            echo $obj;
        }
    }
    
    /**
     * To log the errors in log file
     */
    public function endScript()
    {
        if (error_get_last()) {
            $this->logger->logError(error_get_last());
        }
    }
}

register_shutdown_function(array(new AddressController(),"endScript"));