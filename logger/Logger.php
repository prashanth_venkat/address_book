<?php

/**
 * Logger class to log errors and queries 
 * 
 */
class Logger
{

    private $info;
    private $error_message;
    private $time;
    
    /**
     * Function to prepare current date and time
     */
    public function __construct()
    {
        $this->time = date("Y-m-d H:i:s");
        $this->date = date("Y-m-d");
    }
    
    /**
     * To log errors in log file, if any
     * 
     * @param mixed $message contains the error message
     */
    public function logError($message)
    {
        $type = $this->findErrorType($message);
        $filename = "php-error-".$this->date.".log";
        $path = "/var/www/html/address_book/logger/error/".$filename;
        if(!file_exists($path)) {
            $handle = fopen($path, 'w+') or die("Unable to open file!");
            chmod($path, 0777);
        }
        if (is_array($message)) {
            $this->error_message = $this->time." $type ".$message['message'].
                " in file ".$message["file"]." in line ".$message['line']."\r\n";
        } elseif (is_string($message)) {
            $this->error_message = $this->time." $type ".$message."\r\n";
        }
        error_log($this->error_message, 3, $path);
    }
    
    /**
     * To find the type of error occurred
     * 
     * @param mixed $message contains the error message
     * 
     * @return string
     */
    public function findErrorType($message)
    {
        if (!isset($message['type'])) {
            return $errorType = "[ERROR]";
        } elseif ($message['type']<= ERROR_LEVEL) {
            switch($message['type']) {
                case 1:
                    $errorType = "[FATAL ERROR]  ";
                    break;
                case 2:
                    $errorType = "[WARNING ERROR]";
                    break;
                case 8:
                    $errorType = "[NOTICE ERROR] ";
                    break;
                default :
                    $errorType = "[ERROR]";
            }
            return $errorType;
        }
    }

    /**
     * To log the queries for each operation
     * 
     * @param string $query
     */
    public function logInfo($query)
    {
        $filename = "db-queries-".$this->date.".log";
        $path = "/var/www/html/address_book/logger/queries/".$filename;
        if(!file_exists($path)) {
                $handle = fopen($path, 'w+') or die("Unable to open file!");
                chmod($path, 0777);
            }
        $message = $this->time. " " .$query."\r\n";
        error_log($message, 3, $path);
    }
}

