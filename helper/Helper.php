<?php
/**
 * FileName : Helper.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

/**
 * Helper class to retrieve details from database
 */
class Helper extends Database
{
    /**
     * To get the countries name from the database
     * 
     * @return array $countryList
     */
    public function getCountry()
    {
        $databaseObject = new Database();
        $columns = array('*');
        $condition = array();
        $countryList = $databaseObject->get($columns, COUNTRY_TABLE, $condition);
        return $countryList;
    }
    
    /**
     * To get the states name for the chose country
     * 
     * @param string $country contains the selected country id
     * 
     * @return array $stateName 
     */
    public function getState($country)
    {
        $columns = array('*');
        $condition = array('country_id' => $country);
        $databaseObject = new Database();
        $stateName = $databaseObject->get($columns, STATE_TABLE, $condition);
        return $stateName;
    }
    
    /**
     * To get the availability status of email
     * 
     * @param string $email contains the entered email
     * 
     * @return string $existence
     */
    public function getAvailabilityStatus($email) 
    {
        $columns = array('*');
        $condition = array('email'=> $email);
        $existence = "Already Exists";
        $databaseObject = new Database();
        $result = $databaseObject->get($columns, USERS_TABLE, $condition);
        if (!empty($result)) {
            return $existence;
        }
    }
    
    /**
     * To retrieve particular address detail
     * 
     * @param string $address_id contains the address id of the selected record
     * @param int    $user_id    contains the id of the current user
     * 
     * @return boolean $result
     */
    public function getAddress($address_id, $user_id)
    {
        $columns = array('*');
        $condition = array(
            "address_id" => $address_id,
            "user_id" => $user_id
            );
        $databaseObject = new Database();
        $result = $databaseObject->get($columns, ADDRESS_TABLE, $condition);
        return $result;
    }

}