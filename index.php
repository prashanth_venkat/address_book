<?php

/**
 * FileName : index.php 
 * Author   :  Prashanth <prashanth.venkat@aspiresys.com>
 * 
 * PHP version : 5.5.9
 */

require 'config.php';

/* @var $uri string */

$uri = $_SERVER['REQUEST_URI'];

/* @var $value array */
$value = explode("/", $uri);

/* @var $indexArray int */
$indexArray = array_search("address_book", $value);
$indexArray++;

if ($value[$indexArray] == "") {
    header("Location:".INDEX."/auth/login");
}

$value = array_slice($value, $indexArray);
$action = strtok($value[1], "?");

$controller = ucfirst($value[0])."Controller"; 
$action = $action."Action";

require_once ROOT_DIRECTORY."/controller/".$controller.".php";

$obj = new $controller();
$obj->$action();