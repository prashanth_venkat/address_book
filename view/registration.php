<?php
include "header.php";
?>
<title>Registration</title>
    <link rel = "stylesheet" type = "text/css" href = "http://localhost/address_book/common/css/registration.css">
    <script type = "text/javascript" src = "http://localhost/address_book/common/js/registerValidate.js"></script>    
    <body>
        <div class = "page-header">
            <h2>Registration</h2>
        </div>
        <div class = "container">
            <form id = "register" method = "post" action = "<?php echo INDEX?>/auth/register">
                <table>
                    <span class = "Error">
                        <?php if (isset($_GET['error']))  {
                            echo $_GET['error'];
                        }
                        ?>
                    </span>
                    <tr>
                        <td>Email</td>
                        <td>
                            <input type = "email" name = "email" id = "email" maxlength = "64" >
                            <span class = "Error" id = "emailError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>
                            <input type = "Password" id = "password" name = "password" onblur = "checkPassword()"  >
                            <span class = "Error" id = "passwordError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class = "passwordHint">Min 8(1 Spl, 1 Caps, 1 Num)</td>
                    </tr>
                    <tr>
                        <td>Confirm Password &nbsp; &nbsp; </td>
                        <td>
                            <input type = "Password" id = "confirmPassword" name = "confirmPassword" onblur = "matchPassword()" >
                            <span class = "Error" id = "matchError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href = "<?php echo INDEX?>/auth/login">Home</a>
                        </td>
                        <td>
                            <input type = "submit" name = "register" id = "register" value = "Sign up" onclick = "registerValidate()">
                            <input type = "reset" onclick = "resetSpan()">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>