<?php 
include "header.php";

$helper = new Helper();
$result = $helper->getCountry();
?>
<title>Address</title>
        <link rel = "stylesheet" type = "text/css" href = "http://localhost/address_book/common/css/address.css">
        <script type ="text/javascript" src = "http://localhost/address_book/common/js/addressValidate.js"></script>
    </head>
    <body>
        <div class = "page-header">
            <h2>Add Details</h2>
        </div>
        <nav class = "navbar navbar-inverse">
            <div class = "container-fluid">
                <ul class = "nav navbar-nav">
                    <li><a href = "./list">List Address</a></li>
                    <li class = "active"><a href = "./add">Add Address</a></li>
                    <li><a href = "<?php echo INDEX ?>/auth/logout">Logout</a></li>
                </ul>
                <ul class = "nav navbar-nav navbar-right">
                    <li class = "active"><a class = "glyphicon glyphicon-user">Welcome  <?php echo $_SESSION["user_name"];?></a></li>
                </ul>
            </div>
        </nav>
        <div class = "container text-center">
            <form id = "address" method = "post" action = "<?php echo INDEX?>/address/add">
                <table class = "table table-condensed">
                    <tr>
                        <?php 
                            if (isset($_GET['error'])) {
                                ?><span class = "Error"><?php echo $_GET['error'];?></span>
                            <?php
                            }
                        ?>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>
                            <input type = "text" id = "name" name = "name" maxlength = "20" onblur = "nameCheck()" required>
                            <span class = "Error" id = "nameError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td>
                            <select id = "age" name = "age" onblur = "ageCheck()">
                                <option></option>
                                <?php
                                for ($iteration=1; $iteration<=100; $iteration++)
                                {
                                    ?>
                                        <option value="<?php echo $iteration;?>"><?php echo $iteration;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <span class = "Error" id = "ageError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Address &nbsp; &nbsp; </td>
                        <td>
                            <textarea id = "address" name = "address" rows = "4" cols = "20" maxlength = "120" onblur = "addressCheck()" required></textarea>
                            <span class = "Error" id = "addressError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Mobile</td>
                        <td>
                            <input type = "text" id = "mobile" name = "mobile" maxlength = "13" onblur = "mobileCheck()" required>
                            <span class = "Error" id = "mobileError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>
                            <select id = "country_id" name = "country_id" onchange = "dropdownRequest()" onblur = "countryCheck()" required>
                                <option></option> 
                                <?php
                                foreach($result as $country) {
                                    ?>
                                    <option value = "<?php echo $country['country_id']?>"><?php echo $country['country_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <span class = "Error" id = "countryError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td>
                            <select id = "state_id" name = "state_id" required>
                                <option></option>
                            </select>
                            <span class = "Error" id = "stateError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>
                            <input type = "text" id = "city" name = "city" maxlength = "20" onblur = "cityCheck()" required>
                            <span class = "Error" id = "cityError"></span>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type = "submit" class="btn btn-success" name = "addAddress" id = "addAddress" value = "Add" onclick = "return addressValidate()">
                            <input type = "reset" class="btn btn-warning" onclick = "resetSpan()">
                            <input type = "button" class = "btn btn-danger" name = "cancel" id = "cancel" value = "Cancel" onclick = "location.href ='/address_book/address/list'">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>