<?php
include "header.php";
?>
<script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        </script>
        <title>Address</title>
        <link rel = "stylesheet" type = "text/css" href = "http://localhost/address_book/common/css/address.css">
        <script type ="text/javascript" src = "http://localhost/address_book/common/js/addressValidate.js"></script>
    </head>
    <body>
        <div class = "page-header">
            <h2>Address Book</h2>
        </div> 
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="./list">List Address</a></li>
                    <li><a href="./add">Add Address</a></li>
                    <li><a href="<?php echo INDEX ?>/auth/logout">Logout</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class = "active"><a class="glyphicon glyphicon-user">Welcome  <?php echo $_SESSION["user_name"];?></a></li>
                </ul>
                </div>
            </div>
        </nav>
        <div class = "container-fluid ">
            <table class = "table table-hover">
                <thead class = "thead-inverse">
                    <span class = "Error "></span>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Mobile</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                    <?php
                        if (!empty($details)) {
                            foreach ($details as $record) {
                                ?>
                                <tr>
                                    <td><input type="checkbox" id = "boxes" class = "messageCheckbox" class = "checkbox-primary" name = "selected[]" value = "<?php echo $record['address_id'];?>" data-toggle = "tooltip" data-placement = "top" title = "Select to delete the addresses">&nbsp;</td>
                                    <td><?php echo $record['name']; ?></td>
                                    <td><?php echo $record['address']; ?></td>
                                    <td><?php echo $record['mobile']; ?></td>
                                    <td><input type = "button" class="btn btn-info" value = "Edit" id = "edit" name = "edit" onclick = "location.href='<?php echo INDEX ?>/address/edit/<?php echo $record['address_id']?>'" data-toggle = "tooltip" data-placement = "top" title = "Edit current address"></td>
                                </tr>
                                <?php
                            }
                        } else { 
                            ?>
                            <tr>
                                <td colspan = "5" align = "center" style = "word-spacing: 10px">No address added yet</td>
                            </tr>
                            <?php
                        }
                    ?>
            </table>
            <?php
            if(!empty($details)){
                ?>
                <button id = "delete" class="btn btn-danger" name = "delete" onclick = "deleteRecords()" disabled>Delete Selected</button>
               <?php
            }
            if (isset($_GET['message'])) {
                ?> <script> alert("<?php echo $_GET['message']?>");</script>
                <?php
            }
            ?>
        </div>
    </body>
</html>

<script>
$('.messageCheckbox').change(function() {
    if ($('.messageCheckbox:checked').length) {
        $('#delete').removeAttr('disabled');
    } else {
        $('#delete').attr('disabled', 'disabled');
    }
});
</script>
    