<?php
include "header.php";
?>
<title>Login</title>
        <link rel = "stylesheet" type = "text/css" href = "<?php echo DIR?>/common/css/login.css">
        <script type ="text/javascript" src = "<?php echo DIR?>/common/js/loginValidate.js"></script>
    </head>
    <body>
        <div class = "page-header">
            <h2>Login</h2>
        </div>
        <div class = "container-fluid" id = "content">
            <form id = "login" method = "post" action = "<?php echo INDEX?>/auth/login" >
                <span class = "Error">
                    <?php if (isset($_GET['message']))  {
                        echo $_GET['message'];
                        }
                    ?>
                </span>
                <div class="form-group">
                    <i class="glyphicon glyphicon-envelope"></i>
                    <label for="email">Email</label>
                    <input type = "email" class = "form-control" name = "email" id = "email" maxlength = "64" required>
                    <span class = "Error" id = "emailError"></span>
                </div>
                <div class="form-group">
                    <i class="glyphicon glyphicon-lock"></i>
                    <label for="password">Password</label>
                    <input type = "password"  class = "form-control" id = "password" size = "20" name = "password"  onblur = "checkPassword()" required>
                    <span class = "Error" id = "passwordError"></span>
                </div>
                <div>
                    <input type = "submit" class="btn btn-primary" onclick = "return loginValidate()" name = "login" id = "login" value = "Login">
                    <input type = "reset" class="btn btn-danger" onclick = "resetSpan()">
                    &nbsp;New User?&nbsp;<a href = "<?php echo INDEX?>/auth/register">Sign up</a>
                </div>
            </form>
        </div>
    </body>
</html>