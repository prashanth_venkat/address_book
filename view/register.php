<?php
include "header.php";
?>
<title>Registration</title>
        <link rel = "stylesheet" type = "text/css" href = "http://localhost/address_book/common/css/registration.css">
        <script type = "text/javascript" src = "http://localhost/address_book/common/js/registerValidate.js"></script>    
    </head>
    <body>
        <div class = "page-header">
            <h2>Registration</h2>
        </div>
        <div class = "container-fluid" id = "content">
            <form id = "register" method = "post" action = "<?php echo INDEX?>/auth/register">
                <span class = "Error">
                    <?php if (isset($_GET['message']))  {
                            echo $_GET['message'];
                        }
                    ?>
                </span>
                <div class = "form-group">
                    <i class = "glyphicon glyphicon-user"></i>
                    <label for = "user_name">Name</label>
                    <input type = "text" class = "form-control" name = "user_name" id = "user_name">
                    <span class = "Error" id = "userError"></span>
                </div>
                <div class = "form-group">
                    <i class = "glyphicon glyphicon-envelope"></i>
                    <label for = "email">Email</label>
                    <input type = "email" class = "form-control" name = "email" id = "email">
                    <span class = "Error" id = "emailError"></span>
                </div>
                <div class = "form-group">
                    <i class = "glyphicon glyphicon-lock"></i>
                    <label for = "password">Password</label>
                    <input type = "password"  class = "form-control" id = "password" name = "password" title = "Min 8 chars(1 Num, 1 Caps, 1 Spl char)">
                    
                    <span class = "Error" id = "passwordError"></span>
                </div>
                <div class = "form-group">
                    <i class = "glyphicon glyphicon-lock"></i>
                    <label for = "confirmPassword">Confirm Password</label>
                    <input type = "Password" class = "form-control" id = "confirmPassword" name = "confirmPassword">
                    <span class = "Error" id = "matchError"></span>
                </div>
                <div>
                    <input type = "submit" class = "btn btn-success" name = "signUp" id = "signUp" value = "Sign up">
                    <input type = "reset" class = "btn btn-danger">
                    <input type = "button" class = "btn btn-default" name = "home" id = "home" value = "Home" onclick = "location.href = '/address_book/auth/login'">
                </div>
            </form>
        </div>
    </body>   
</html>